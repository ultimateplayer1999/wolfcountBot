require('dotenv').config();

const mongoose = require('mongoose');
const Wolfcount = require('./models/wolfcount');

const dblocation = process.env.DBLOCATION

try {
  
  if (dblocation === 'selfhosted') {
    const uri = `mongodb://${process.env.MONGODBUSER}:${process.env.MONGODBPASS}@${process.env.MONGODBHOST}/${process.env.DATABASE}`    
    mongoose.connect(`${uri}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }).then(() => {
      console.log('');
      console.log(`Connected to the selfhosted database`);
    }).catch((error) => {
      console.log(`Failed to connect to the database`);
      console.log(error);
    });
  } else if (dblocation === 'cloud') {
    const uri = `mongodb+srv://${process.env.MONGODBUSER}:${process.env.MONGODBPASS}@${process.env.MONGODBCLUSTER}/${process.env.DATABASE}?retryWrites=true&w=majority`
    
    mongoose.connect(`${uri}`, {
          useNewUrlParser: true,
          useUnifiedTopology: true
        }).then(() => {
          console.log('');
          console.log(`Connected to the database`);
        }).catch((error) => {
          console.log(`Failed to connect to the database`);
          console.log(error);
        });
      } else {
        console.error(`Failed to connect to the database, it is possible that the host option is not available`);
      }
    
    // Wait a few seconds bfore execution
    setTimeout(function() {
        // ensure that the bot stats document exists
        const wolfcount = new Wolfcount({
            count: 0,
            validation: "wolfcount"
        });

        wolfcount.save().then(() => {
            console.log('Count document saved successfully into the database!');
            Wolfcount.find().then((document) => {
                console.log(document);
            }).catch((err) => {
                console.log('Failed to retrieve document from the database', err)
            });
        }).catch(err => {
            console.log('Error while saving the count document', err);
        });
    }, 3000);
    setTimeout(function() {
        try {mongoose.disconnect();}
        catch(err){console.log('Error while disconnecting from the database', err);}
    }, 3000);
    setTimeout(function() {
        console.log('Collection and document saved successfully into the database. Succesfully closed the connection to the db!');
    }, 4000);
}
catch(err) {
    console.log('Error while connecting to the database', err);
}