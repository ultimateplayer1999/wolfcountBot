require("dotenv").config();
const { ActivityType } = require("discord.js");
const { glob } = require("glob");
const { promisify } = require("util");
const globPromise = promisify(glob);
const mongoose = require('mongoose');
const wolfcount = require('../models/wolfcount');
var cron = require('node-cron');
const moment = require('moment');
const express = require("express");
const app = express();
const port = process.env.PORT
const cwd = process.cwd()

module.exports = async (client) => {

  
  const dblocation = process.env.DBLOCATION
  if (dblocation === 'selfhosted') {
    const uri = `mongodb://${process.env.MONGODBUSER}:${process.env.MONGODBPASS}@${process.env.MONGODBHOST}/${process.env.DATABASE}`    
    mongoose.connect(`${uri}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }).then(() => {
      console.log('');
      console.log(`Connected to the selfhosted database`);
    }).catch((error) => {
      console.log(`Failed to connect to the database`);
      console.log(error);
    });
  } else if (dblocation === 'cloud') {
    const uri = `mongodb+srv://${process.env.MONGODBUSER}:${process.env.MONGODBPASS}@${process.env.MONGODBCLUSTER}/${process.env.DATABASE}?retryWrites=true&w=majority`
    
    await mongoose.connect(`${uri}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }).then(() => {
      console.log('');
      console.log(`Connected to the database`);
    }).catch((error) => {
      console.log(`Failed to connect to the database`);
      console.log(error);
    });
  } else {
    console.error(`Failed to connect to the database, it is possible that the host option is not available`);
  }

  // Slash Commands
  const slashCommands = await globPromise(`${process.cwd()}/commands/*/*.js`);
  const arrayOfSlashCommands = [];
  slashCommands.map((value) => {
    const file = require(value);
    const splitted = value.split("/");
    const directory = splitted[splitted.length - 2];

    if (!file?.name) return;

    const properties = { directory, ...file };
    client.slashCommands.set(file.name, properties);

    if (["MESSAGE", "USER"].includes(file.type)) delete file.description;
    arrayOfSlashCommands.push(file);
  });

  // Events
  const eventFiles = await globPromise(`${process.cwd()}/events/*.js`);
  eventFiles.map((value) => require(value));

  // Find a document by validation
  wolfcount.findOne({ validation: 'wolfcount' }).then((countdocument) => {
    console.log('');
    console.log(countdocument);
    basecount = countdocument.count || 0;

    console.log('');
    console.log(`Starting count from ${basecount}`);
    console.log(``);
  }).catch((error) => {
    console.error(error);
  });

  //   setInterval(function() {
  //     // Find a document by validation
  //     wolfcount.findOne({ validation: 'wolfcount' }).then((countdocument) => {
  //         console.log('');
  //         //console.log(countdocument);
  //         console.log(`Refreshed count`)
  //         basecount = countdocument.count

  //         console.log('');
  //     }).catch((error) => {
  //         console.error(error);
  //     });
  // }, 30000);

  // Slash Commands Register
  client.on("ready", async () => {

    // client.user.setPresence({
    //   activities: [{ name: `over msgs for certain letters`, type: ActivityType.Watching }],
    //   status: 'dnd',
    // });

    cron.schedule('*/2 * * * *', async () => {
      console.log(`${moment(Date.now()).format('DD/MM/YY H:mm:ss')} Starting count refresh...`);
  
      // Find a document by validation
      wolfcount.findOne({ validation: 'wolfcount' }).then((countdocument) => {
        console.log('');
        //console.log(countdocument);
        console.log(`Refreshed count`)
        let basecount = countdocument.count;
  
        console.log('');
        return basecount;
      }).catch((error) => {
          console.error(error);
      });
  
      console.log(`${moment(Date.now()).format('DD/MM/YY H:mm:ss')} Refreshed wolfcount on cron`);
    });

    // Register for a single guild
    // await client.guilds.cache.get("239352426451304449").commands.set(arrayOfSlashCommands);
    // Register for a single guild
    // await client.guilds.cache.get("1075800713323610142").commands.set(arrayOfSlashCommands);

    // Register for all the guilds the bot is in
    await client.application.commands.set(arrayOfSlashCommands);
  });

  // Error handling
  client.on("error", async (error) => {
    console.log('');
    console.error("Something went wrong:", error);
  });

  app.set('view engine', 'ejs');
  app.use(express.static(`${cwd}/views/images`));

  // The root of the webapp
  app.get("/", (req, res) => {
    res.render('index', {basecount});
  });

  app.get("/info", (req, res) => {
    res.render('info');
  });

  app.get("/privacy", (req, res) => {
    res.render('privacy');
  });

  app.get("/tos", (req, res) => {
    res.render('tos');
  });

  app.get("/donate", (req, res) => {
    res.render('donate');
  });

  // redirect /invite to the discord invite
  app.get('/invite', async (req, res) => {
    res.redirect(`https://discord.com/oauth2/authorize?client_id=${client.user.id}&permissions=327744&scope=bot%20applications.commands`);
  });

  app.listen(port, () => {
    console.log(`Webapp listening on port ${port}`)
  });

};
