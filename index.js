require("dotenv").config();
const { Client, Collection, Options } = require("discord.js");

const client = new Client({ 
    intents: 34305,
    makeCache: Options.cacheWithLimits({MessageManager: 0})
    // sweepers: {
    //     ...Options.DefaultSweeperSettings,
    //     messages: {
    //         interval: 120, // Every 2 minutes...
    //         lifetime: 600,  // Remove messages older then 10 minutes.
    //     },
    // },
});
module.exports = client;

// Global Variables
client.commands = new Collection();
client.slashCommands = new Collection();

console.clear();
console.log('');
console.log(`Welcome to the console of wolfcount /--/ By Ultimateplayer1999 /--/ Discord: ultimateplayer#0369`);

// Initializing the project
require("./handler")(client);

client.login(process.env.TOKEN);
