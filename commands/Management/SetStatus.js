const { EmbedBuilder, SlashCommandBuilder } = require('discord.js');
const client = require('../../index.js');

developerList = [
    '213966480621043712',
];

module.exports = {
    name: "setstatus",
    private: true,
    description: "Set status",
  
    run: async (client, interaction) => {
        if (developerList.includes(interaction.user.id)) {

            client.user.setStatus('dnd')

            const embed = new EmbedBuilder()
            .setColor("#FF0000")
            .setTitle("Status set")
            .setDescription(`Bot status set to predefined value`)
            .setTimestamp()
            .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
          interaction.followUp({ embeds: [embed] });

        } else {
            const embed = new EmbedBuilder()
            .setColor("#FF0000")
            .setTitle("🐺 no perms")
            .setDescription(`You have no perms to do this.`)
            .setTimestamp()
            .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
          interaction.followUp({ embeds: [embed] });
        }
    },
};
