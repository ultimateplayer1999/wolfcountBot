const { EmbedBuilder } = require('discord.js');
const wolfcount = require('../../models/wolfcount');
const { spawn } = require("child_process");

ownerList = [
    '213966480621043712',
    '237542879227019264',
    '294840381080600576'
];

module.exports = {
    name: "wolfcountreset",
    private: true,
    description: "Reset the wolfcount",
  
    run: async (client, interaction) => {
        if (ownerList.includes(interaction.user.id)) {

            resetcount = 0

            // Reset the wolf count
            wolfcount.findByIdAndUpdate(`${process.env.UPDATEID}`, {count: resetcount}).then(() => {
                console.log('');
                console.log('Count reset!');
                }).catch((error) => {
                console.error(error);
            });

            const embed = new EmbedBuilder()
            .setColor("#FF0000")
            .setTitle("🐺 count reset")
            .setDescription(`The count has been reset. (Requested by: ${interaction.user.tag})`)
            .setTimestamp()
            .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
          interaction.followUp({ embeds: [embed] });

          basecount = resetcount;
          
        } else {
            const embed = new EmbedBuilder()
            .setColor("#FF0000")
            .setTitle("🐺 no perms")
            .setDescription(`You have no perms to do this.`)
            .setTimestamp()
            .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
          interaction.followUp({ embeds: [embed] });
        }
    },
};