const { EmbedBuilder } = require('discord.js');

module.exports = {
    name: "wolfinv",
    private: false,
    description: "Returns the invite of wolfcount.",
  
    run: async (client, interaction) => {

        const embed = new EmbedBuilder()
            .setColor("#03FC20")
            .setTitle("🐺 count")
            .setDescription(`[invite botcounter](https://discord.com/oauth2/authorize?client_id=1100390424381751328&permissions=327744&scope=bot%20applications.commands)`)
            .setTimestamp()
            .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
        interaction.followUp({ embeds: [embed] });
    },
};