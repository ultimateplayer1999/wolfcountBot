const { EmbedBuilder } = require('discord.js');

module.exports = {
    name: 'donate',
    private: false,
    description: 'Shows links to donate to the service',

    run: async (client, interaction) => {

        const embed = new EmbedBuilder()
            .setColor('#03FC20')
            .setTitle('Donation')
            .setDescription('Donate to the service I am using with one of the following links:')
            .setFields(
                { name: ' ', value: '[Direct donation to the host (creditcard needed)](https://ssh.surf/view/donate/)', inline: false },
                { name: ' ', value: '[Donation through an external provider](https://ko-fi.com/sshsurf)', inline: false },
            )

        interaction.followUp({ embeds: [embed] })
    }
};