require("dotenv").config();
const { EmbedBuilder } = require('discord.js');
const mongoose = require('mongoose');
const wolfcount = require('../../models/wolfcount');

// const dblocation = process.env.DBLOCATION
// if (dblocation === 'selfhosted') {

//   await mongoose.connect('mongodb://127.0.0.1:27017/wolfcounter', {
//     useNewUrlParser: true,
//     useUnifiedTopology: true
//   }).then(() => {
//     console.log('');
//     console.log(`Connected to the database`);
//   }).catch((error) => {
//     console.log(`Failed to connect to the selfhosted database`);
//     console.log(error);
//   });
// } if (dblocation === 'cloud') {

//   await mongoose.connect(`mongodb+srv://${process.env.MONGODBUSER}:${process.env.MONGODBPASS}@${process.env.MONGODBCLUSTER}/${process.env.DATABASE}?retryWrites=true&w=majority`, {
//     useNewUrlParser: true,
//     useUnifiedTopology: true
//   }).then(() => {
//     console.log('');
//     console.log(`Connected to the database`);
//   }).catch((error) => {
//     console.log(`Failed to connect to the database`);
//     console.log(error);
//   });
// } else {
//   console.error(`Failed to connect to the database, it is possible that the host option is not available`);
// }

// setInterval(function() {
//     // Find a document by validation
//     wolfcount.findOne({ validation: 'wolfcount' }).then((countdocument) => {
//         console.log('');
//         //console.log(countdocument);
//         console.log(`Refreshed count`)
//         count = countdocument.count

//         console.log('');
//     }).catch((error) => {
//         console.error(error);
//     });
// }, 30000);


module.exports = {
    name: "wolfcount",
    private: false,
    description: "Shows the wolfcount.",
  
    run: async (client, interaction) => {

        const embed = new EmbedBuilder()
            .setColor("#03FC20")
            .setTitle("🐺 count")
            .setDescription(`[I have counted ${basecount} wolf messages](https://wolfcount.skywolfteam.nl/)`)
            .setTimestamp()
            .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
        interaction.followUp({ embeds: [embed] });
    },
};