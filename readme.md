# A bot that counts that counts how many the letters where used in messages. It also reacts to the messages that have the letters in it.
---
The configuration is based on a `.env` file. See the example.env file for the configuration options.
It is currently only based on the cloud version of mongodb.

This bot is based on [shrimpBot](https://github.com/numselli/shrimpBot). Converted the DB from postgres to mongodb.

Use the following command to initialize the database.
`node dbinit.js`
It will create the needed collection and document and return it in the console output.

If you want to run it yourself, use the output of _id for the search and update operation. The following part is needed for this
"6448db015eed6ed191ef61a1" (This is offcourse an example, yours will be different.)

Structure: 

**commands** - This folder contains commands

**event** - This folder contains files related to discord.js events. (Like "ready", "interactionCreate")

**handler**  - This folder contains files that read the commands folders contents.

**models** - This folder contains files related to the database models. (Like "wolfcount")

**index.js** - This is the main file to run the bot.



1) Use ```npm i ```

2) Create a .env file ``` touch .env``` or ``` cp example.env .env ```

3) Edit .env 
```
# BASIC BOT INFO
TOKEN=<bottoken>

#Mongodb
DBLOCATION=cloud # or selfhosted
DB_URL=<optional>
MONGODBUSER=<Username>
MONGODBPASS=<Pass>
MONGODBCLUSTER=<cluster>
MONGODBHOST=<host> # Required if selfhosted is chosen
DATABASE=<dbname>
UPDATEID=<documentid> # This is the ID of the document which is created with the dbinit (or the ID of the document of an existing document if you are reinstalling this)

#Webstuff
PORT=<port>
```

4) Go to Handler -- > index.js and change "GUIDIDHERE" to your Discord Server's Guild ID for guild only commands.

5) Go into https://discord.com/developers/applications and enable Privileged Message Intent (If your bot is in more that 100 guilds this requires verification and approval of Discord.).

6) Run the bot ```node index.js```


# Want to donate? Please use one of the following links below to support the platform I am using.
[Direct donation to the host (creditcard needed)](https://ssh.surf/view/donate/)
---
[Donation through an external provider](https://ko-fi.com/sshsurf)


Want to make this better? Issue a pull request!
