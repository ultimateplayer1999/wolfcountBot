const client = require("../index");
const wolfcount = require('../models/wolfcount');

// When a message is sent, check if the message includes the letters of wolf
const wolfChars = ['w', 'o', 'l', 'f'];

client.on("messageCreate", async (message) => {
    if (message.author.bot) return;
    if (message.content.includes(">>") || message.content.includes(">r>")) return;
    
    // convert the message to lowercase
    const lowerMsg = message.content.toLowerCase();
    // console.log(lowerMsg);
  
    // check if the message has the letters
    if (wolfChars.every(char=>lowerMsg.includes(char))) {
      
        message.react("🐺").catch(err=>console.log(err));
  
        newcount = basecount + 1;
        // client.user.setPresence({
        //     // activities: [{ name: `${newcount} message(s) detected`, type: ActivityType.Watching }],
        //     activities: [{ name: `${newcount} message(s) detected` }],
        //     status: 'dnd',
        // });
        basecount = newcount;
  
        console.log('');
        console.log('Live wolf count:', newcount);

        if (newcount == 69) {
          message.react("1103666748592488549").catch(err=>console.log(err)); // pogslide
          message.react("1103673748466434140").catch(err=>console.log(err)); // 69nice
        }

        if (newcount == 100) {
          message.react("💯").catch(err=>console.log(err));
          message.react("1103666748592488549").catch(err=>console.log(err)); // pogslide
        }

        if (newcount == 420) {
          message.react("1103666748592488549").catch(err=>console.log(err)); // pogslide
          message.react("1103675067054948392").catch(err=>console.log(err)); // pepe_high
        }
  
        // update the wolf count
        wolfcount.findByIdAndUpdate(`${process.env.UPDATEID}`, {count: newcount}).then(() => {
            console.log('Count updated!');
          }).catch((error) => {
            console.error(error);
        });
    
    }
  });