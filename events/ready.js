const client = require("../index");
const { EmbedBuilder, ActivityType } = require('discord.js');


client.on("ready", () => {
  console.log(`${client.user.tag} is up and ready to go!`);

  // set the status to do not disturb
  // client.user.setStatus('dnd')
  // client.user.setPresence({
  //   activities: [{ name: `over msgs for certain letters`, type: ActivityType.Watching }],
  //   status: 'dnd',
  // });

    client.user.setPresence({
    activities: [{ name: `RIP #1123`, type: ActivityType.Playing }],
    status: 'dnd',
  });

  // Function to convert RGB to hex
  function convertToHex(rgb) {
    // Extract the RGB values using a regex pattern
    const rgbValues = rgb.match(/\d+/g);

    // Convert each RGB value to a hexadecimal string
    const hexValues = rgbValues.map((value) => {
      // Convert the value to a number
      const intValue = parseInt(value);

      // Convert the number to a hexadecimal string
      const hexString = intValue.toString(16);

      // Pad the string with zero if needed
      return hexString.padStart(2, '0');
    });

    // Return the hex color string
    return `#${hexValues.join('')}`;
  };

  const readyEmbed = new EmbedBuilder()
    .setTitle('Now online!')
    .setColor(convertToHex('rgb(119, 152, 229)'))
    .setAuthor({ name: client.user.tag, iconURL: client.user.avatarURL() })
    .setTimestamp()
    .setDescription(`${client.user.tag} is nu online`);

  // client.channels.cache.get(`967754078438449192`).send(`${client.user.tag} is nu online.`);
  client.channels.cache.get(`967754078438449192`).send({ embeds: [readyEmbed] })

});
