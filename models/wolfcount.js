const mongoose = require('mongoose');

const wolfcountSchema = new mongoose.Schema({
    count: Number,
    validation: String
});

const Wolfcount = mongoose.model('Wolfcount', wolfcountSchema);

module.exports = Wolfcount;